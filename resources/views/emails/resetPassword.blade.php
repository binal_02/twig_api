<table class="m_7004521440052286999wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box;background-color:#f5f8fa;margin:0;padding:0;width:100%">
    <tbody>
        <tr>
            <td align="center" style="font-family:El Messiri,sans-serif;box-sizing:border-box">
                <table class="m_7004521440052286999content" width="100%" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box;margin:0;padding:0;width:100%">
                    <tbody>
                        <tr>
                            <td class="m_7004521440052286999header" style="font-family:El Messiri,sans-serif;box-sizing:border-box;padding:25px 0;text-align:center">
                                <a href="<?php echo url('/');?>" style="font-family:El Messiri,sans-serif;box-sizing:border-box;color:#bbbfc3;font-size:19px;font-weight:bold;text-decoration:none" target="_blank" >
                                    TWiG
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="m_7004521440052286999body" width="100%" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box;background-color:#ffffff;border-bottom:1px solid #edeff2;border-top:1px solid #edeff2;margin:0;padding:0;width:100%">
                            <table class="m_7004521440052286999inner-body" align="center" width="727" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 auto;padding:0;width:727px">
                                    <tbody>
                                        <tr>
                                            <td class="m_7004521440052286999content-cell" style="font-family:El Messiri,sans-serif;box-sizing:border-box;padding:35px">
                                                <h1 style="font-family:El Messiri,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">Welcome <?php if($userInfo['FirstName'] != ''){echo $userInfo['FirstName'];}else{echo 'Devon';} ?>,</h1>
                                                <p style="font-family:El Messiri,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">You are receiving this email because we received a password reset request for your account.</p>
                                                <table  align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box;margin:30px auto;padding:0;text-align:center;width:100%">
                                                    <tbody><tr>
                                                            <td align="center" style="font-family:El Messiri,sans-serif;box-sizing:border-box">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box">
                                                                    <tbody><tr>
                                                                            <td align="center" style="font-family:El Messiri,sans-serif;box-sizing:border-box">
                                                                                <table border="0" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box">
                                                                                    <tbody><tr>
                                                                                <td style="font-family:El Messiri,sans-serif;box-sizing:border-box">
        <a href="<?php echo url('/').'/resetPassword?token='.$tokenarray[0].'/'.$userInfo['email'] ?>" target="_blank" style="font-family:El Messiri,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1">Click Here To Signup</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody></table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <p style="font-family:El Messiri,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">If this was a mistake, just ignore this mail and nothing will happen.</p>
                                                <p style="font-family:El Messiri,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">Please reach out to us at <a href="support@bloxian.com" target="_blank">support@bloxian.com</a> for any queries or any assistance required.</p>
                                                <p style="font-family:El Messiri,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">Yours sincerely,<br>TWiG team</p>
                                                </td>
                                        </tr>
                                    </tbody>
                            </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family:El Messiri,sans-serif;box-sizing:border-box">
                                <table class="m_7004521440052286999footer" align="center" width="727" cellpadding="0" cellspacing="0" style="font-family:El Messiri,sans-serif;box-sizing:border-box;margin:0 auto;padding:0;text-align:center;width:727px">
                                    <tbody><tr>
                                        <td class="m_7004521440052286999content-cell" align="center" style="font-family:El Messiri,sans-serif;box-sizing:border-box;padding:35px">
                                            <p style="font-family:El Messiri,sans-serif;box-sizing:border-box;line-height:1.5em;margin-top:0;color:#aeaeae;font-size:12px;text-align:center">(Powered by Bloxian Technology)</p>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

