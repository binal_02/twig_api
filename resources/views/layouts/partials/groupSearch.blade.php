<div class="row" style="margin:0">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default" style="margin-bottom: 5px;">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$group->_id}}" aria-expanded="true" aria-controls="collapseOne">
                        {{$group->Name}}
                        <div class="btn-float">
                            <button type="button" class="btn btn-flat btn-warning addUser" data-toggle="modal" data-target="#myModalAddUser{{$group->_id}}">Add User</button>
                            <form action="{{ url('/admin/group/'.$group->id) }}" method="post" style="display: none" id="formdel{{$group->_id}}">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <input name="group" type="hidden" value="groupD">
                                <button class="btn btn-flat btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                            </form>
                            <button type="button" class="btn btn-flat btn-danger" data-toggle="modal" id="deleteGroup{{$group->_id}}">{{ trans('message.delete group')}}</button>

                        </div>
                    </a>
                </h4>
            </div>
            <div id="collapse{{$group->_id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example{{$group->_id}}" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email Address</th>
                                                <th>Role</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(App\User::getUsersFromGroupId($group->_id) as $user)
                                            <tr>
                                                <td>{{$user->FirstName}} {{$user->LastName}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{App\Role::gerRoleName($user->RoleID)}}</td>
                                                <td><button class="btn btn-flat btn-primary edituser" data-title="Edit" data-id="{{ $user->id }}" data-toggle="modal" data-target="#edit">
                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                    </button>
                                                </td>
                                                <td>
                                                    <form action="{{ url('/admin/group/'.$user->id) }}" method="post" onsubmit='return confirmDelete()'>
                                                        @csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <input name="user" type="hidden" value="userD">
                                                        <button class="btn btn-flat btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
        </div>

        <div id="myModalAddUser{{$group->_id}}" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 400px;padding-top: 7%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <center><h2 class="modal-title">Add User</h2></center>
                    </div>
                    <div class="modal-body" style="overflow: hidden;">
                        <form method="POST" role="form" aria-label="{{trans('message.Enter Group Name')}}" id='formGroup{{$group->_id}}' enctype='multipart/form-data'>
                            @csrf
                            <input type="hidden" name="GroupID" value="{{$group->_id}}" class="" />
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="" style="font-weight: 700;padding-right: 15px;font-size: 16px;">Role : </label>
                                    @foreach($roles as $role)
                                    @if($role->Name !== 'SuperAdmin')
                                    <label>
                                        <input type="radio" name="RoleID" class="flat-yellow RoleID" value='{{$role->_id}}' required>

                                        <div style="display: inline;padding-right: 10px;font-weight: 500;"> {{$role->Name}} </div>
                                    </label>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="form-group">
                                    <label for="csvImport" style="font-weight: 700;font-size: 16px;">{{trans('message.choose csv file')}}</label>
                                    <input type="file" name="csvImport" class="form-control" id="csvImport">
                                </div>
                                <div class="center-block" style="margin-bottom: 15px;">
                                    <center><h3>OR</h3></center>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="firstName" class="form-control" id="firstName{{$group->_id}}" placeholder="First Name" value="{{ old('firstName') }}">
                                            <span class="glyphicon glyphicon-user form-control-feedback" style="margin-right: 14px;"></span>
                                        </div>
                                        <div class="firstNameErrorMessages" style="display: none;">
                                            <span id="" style="color:red;">
                                                The first name field is required.
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="lastName" class="form-control" id="lastName{{$group->_id}}" placeholder="Last Name" value="{{ old('lastName') }}">
                                            <span class="glyphicon glyphicon-user form-control-feedback" style="margin-right: 14px;"></span>
                                        </div>
                                        <div class="lastNameErrorMessages" style="display: none;">
                                            <span id="" style="color:red;">
                                                The last name field is required.
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input id="email{{$group->_id}}" type="text" class="form-control email" placeholder="Email" name="email" value="{{ old('email') }}" pattern="/^[w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/" >
                                        <span class="glyphicon glyphicon-envelope form-control-feedback" style="margin-right: 14px;"></span>
                                    </div>
                                </div>
                                <div class="EmailErrorMessages" style="display: none;">
                                    <span id="" style="color:red;">
                                        Please enter valid Email Address
                                    </span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-flat btn-warning" id='submitUserForm{{$group->_id}}'>{{trans('message.Add User')}}</button>
                                <button type="button" class="btn btn-flat btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ url('admin/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{ url('admin/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script> 
        <script>
            
            $('#example{{$group->_id}}').DataTable({
            'paging': true,
                    'lengthChange': true,
                    'searching': false,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
            })
                    function confirmDelete() {
                    return confirm('Are you sure you want to delete user?');
                    }
            $("#deleteGroup{{$group->_id}}").click(function(){
            var r = confirm('Are you sure you want to delete group ?');
            if (r == true) {
            $("#formdel{{$group->_id}}").submit();
            }
            });
            $('body').on('click', '#submitUserForm{{$group->_id}}', function () {
            var formGroup{{$group -> _id}} = $("#formGroup{{$group->_id}}");
            var formDataUser = formGroup{{$group -> _id}}.serialize();
            var emailerror = 0;
            var input = document.getElementById('email{{$group->_id}}');
            input.oninvalid = function(event) {
            event.target.setCustomValidity('Please Enter Valid Email address');
            }
//                        var email = $(".email").val();
//                        if (email == ""){
//                             $(".EmailErrorMessages").css("display", "block"); 
//                        }else{
//                            if (validateEmail(email)) {
            $.ajax({
            url:'admin/group',
                    type:'POST',
                    data:formDataUser,
                    datatype:'json',
                    success:function(data) {
                    console.log(data);
                    },
            });
//                            }else {
////                                 $(".EmailErrorMessages").css("display", "block");
//                                 alert('Invalid Email Address');
//                            }
            });
//                        if($('.email').val() == ''){
//                            $(".EmailErrorMessages").css("display", "block");     
//                            emailerror = 1;
//                        }else{
//                            emailerror = 0;
//                                       
//                        }

//                    });
            function validateEmail(email) {
            var filter = /^[w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
            if (filter.test(email)) {
            return true;
            } else {
            return false;
            }
            }
            $(document).on('click', '.edituser', function() {
            var user_id = $(this).data('id');
            $.ajax({
            url: 'group/' + user_id + '/edit',
                    type: 'GET',
                    dataType: 'JSON',
                    success:function(data) {
                    console.log(data);
                    //                              var name = data.name;   
                    var modal = $('#edit');
                    //                                modal.find('.modal-title').text('Information for Passenger ' + name);
                    modal.find('#firstName').val(data.FirstName);
                    modal.find('#lastName').val(data.LastName);
                    modal.find('#email').val(data.email);
                    modal.find("input[name='RoleID'][value='" + data.RoleID + "']").prop('checked', true);
                    modal.find("#myGroupID").val(data.GroupID);
                    modal.find("#oldGroupId").val(data.GroupID);
                    modal.find("#UserID").val(user_id);
                    $('#edit').modal('show');
                    },
            });
            });
        </script>
    </div>
</div>