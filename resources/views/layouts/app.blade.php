<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       
        <link href="{{ asset('img/MyIcon.ico')}}"  rel="shortcut icon" type="image/x-icon" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=El+Messiri" rel="stylesheet">
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body style="background-color: #222d32;">
        <div id="app" style="overflow-x: hidden;">
            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </body>
</html>
