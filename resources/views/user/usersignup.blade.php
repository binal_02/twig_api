@extends('layouts.app')

@section('content')


<div class="row py-2 justify-content-center">
    <div class="col-md-12 col-lg-6  col-md-8 ">
        <div class="input-group justify-content-center" style="padding-bottom:5%;padding-top: 3%;">
            <img src="{{ url('img/test1.png')}}" alt="bloxian" height="155" width="92">
        </div>
        <div class="input-group justify-content-center" >
            <span style="color: white;font-size: x-large;">Register for TWiG</span>
        </div>
        <div class="input-group justify-content-center" style="margin-bottom: 10px;">
            <span style="color: white;font-size: small;">Enter below details to register</span>
        </div>
        @if (session('status'))

        <div class="alert alert-success" role="alert" style="margin-left: 28%;width: 44%;">
            {{ session('status') }}
        </div>
        @endif
        <form action="{{ url('/user/'.$userInfo->_id)}}" method="POST" id="UserRegistrationForm">
            @csrf

            <input name="_method" type="hidden" value="PUT">
            <input name="SessionID" type="hidden" value="{{$userSession->SessionID}}" />
            <input name="UserSessionID" type="hidden" value="{{$userSession->UserSessionID}}" />
            <div class="input-group" style="width: 72%;padding-left: 28%;padding-bottom: 2%;">
                <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{$userInfo->email}}" readonly="">

            </div>
            <div class="input-group" style="width: 72%;padding-left: 28%;padding-bottom: 2%;">
                <input type="text" name="firstName" class="form-control" id="firstName" placeholder="First Name" value="{{$userInfo->FirstName}}" >
            </div>
            <div class="input-group" style="width: 72%;padding-left: 28%;padding-bottom: 2%;">
                <input type="text" name="lastName" class="form-control" id="lastName" placeholder="Last Name" value="{{$userInfo->LastName}}" >
            </div>
            <div class="input-group" style="width: 72%;padding-left: 28%;padding-bottom: 2%;">
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" value="">
            </div>
            <div class="input-group" style="width: 72%;padding-left: 28%;padding-bottom: 2%;">
                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" value="">
            </div>

            <div class="input-group" style="width: 72%;padding-left: 28%;padding-bottom: 2%;">
                <span style="color: white;">
                    (Password must contain at least 6 characters, one upper case letter, one lower case letter, and one numeric digit.)</span>
            </div>
            <div class="input-group" style="width: 72%;padding-left: 28%;padding-bottom: 2%;">

                <button type="submit" class="btn btn-responsive btn-block btn-flat btn-warning" style="border-radius: .25rem;" id='submitUserEditForm' title="Register">Register</button>
                </button>
            </div>
        </form>

        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-8">
                @if (\Session::has('error'))
                <div class="alert alert-error notifications-msg" >
                    <a href="#" class="close notifications-close" data-dismiss="alert" aria-label="close" >&times;</a>
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger notifications-msg">
                    <a href="#" class="close notifications-close" data-dismiss="alert" aria-label="close" >&times;</a>
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
                @endif
            </div>
        </div>

        <!--	@if ($errors->has('email'))
<div class="alert alert-danger" style="margin-left: 28%;width: 44%;">
<span class="" role="alert">
    <strong>{{ $errors->first('email') }}</strong>
</span>
</div>
@endif
@if ($errors->has('password'))
<div class="alert alert-danger" style="margin-left: 28%;width: 44%;">
<span class="" role="alert">
    <strong>{{ $errors->first('password') }}</strong>
</span>
</div>
@endif -->
    </div>
</div>
@endsection
