@extends('layouts.app')
@section('content')
<div class="row py-2 justify-content-center">
    <div class="col-12 col-lg-6  col-md-8 ">
        <div class="input-group justify-content-center" style="padding-bottom:10%;padding-top: 5%;">
            <img src="{{ url('img/test1.png')}}" alt="bloxian" height="155" width="92">
        </div>
        <div style="">
            <center><span style="color:white"><h2>{{$message}}</h2></span><br>
                <button onclick="window.close();" class="btn btn-warning btn-lg">Close</button></center>
        </div>
    </div>
</div>  
@endsection