<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,  X-Requested-With, Access-Control-Allow-Headers, Authorization');
header('Access-Control-Max-Age: 1000');
header("Access-Control-Allow-Credentials: true");

Route::post('userLogin', 'LoginController@userLogin');
Route::post('forgotPassword', 'ForgotPasswordController@forgotPassword');