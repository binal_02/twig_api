<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Auth;
use Hash;
use App\User;
use Session;
use Cookie;
use App\TwigSession;
use App\UserSession;
use Illuminate\Http\Response;
use MongoDB\BSON\ObjectID;
use Carbon\Carbon;

class LoginController extends Controller {
    
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function userLogin(Request $request)
    {
        $success = $error = '';
//        $validatedData = $request->validate([
//            'email' => 'required',
//            'password' => 'required',
//        ]);
//        if ($validator->passes()) {
           
           if($request->email == ''){
               $error = 'The email field is required';
               return response()->json(['errors' => $error], 400 );
           }
           if($request->password == ''){
               $error = 'The password field is required';
               return response()->json(['errors' => $error], 400 );
           }
        
            $sessionId = $request->sessionId;
            $sessionID = New ObjectID($sessionId);
            $session = TwigSession::where('_id', $sessionID)->first();
            $SessionStartTime = $session['StartTimeLong'];
            
            if(isset($session) && $session != ''){
                date_default_timezone_set("UTC");
                $startTime = Carbon::now(); 

                $diff = $SessionStartTime - strtotime($startTime);
                
                if($diff < (900)){
                    $UserSession = UserSession::where('SessionID', $sessionID)
                            ->where('Password', $request->password)
                            ->first();
                    if(!empty($UserSession)){
//                        echo $request->password;echo "<br>";
//                        echo $UserSession->Password;
//                        if (Hash::check(trim($request->password), $UserSession->Password)) {
                            $userId = $UserSession->UserID;
                            $userInfo = User::find(New ObjectID($userId));
                            if(!empty($userInfo) && $userInfo->email == $request->email){
                                $success = 'welcome';
                                $name = $userInfo->FirstName;
                                return response()->json($name, 200);
                            }else{
                                 $error = 'The email/password combination is not valid. Please try again.';
                                 return response()->json(['errors' => $error], 400 );
                            }
//                        }else{
//                            $error = 'The email/password combination is not valid. Please try again.';
//                            return response()->json(['errors' => $error], 400 );
//                        }
                        
                    }else{
                       $error = 'The email/password combination is not valid. Please try again';
                       return response()->json(['errors' => $error], 400 );
                    }
                }else{
                    $error = 'A User (Devon) may login in to the application 15 minute prior to Start session';
                    return response()->json(['errors' => $error], 400 );
                }
            }else{
                $error = 'SessionId is not valid,Please try again';
                return response()->json(['errors' => $error], 400 );
            }
            
//          
//        }
//        $validator = $validator->errors();
//        return response()->json(['errors' => $validator], 400 );
    }
    
    public function login(LoginRequest $request) {
        $this->validateLogin($request);
        // $response = new Response;
        $remember_me = $request->has('remember');
        if($remember_me == '1'){
            Cookie::queue('email', $request->email, 360000);
            Cookie::queue('password', $request->password, 360000);
        }else{
            Cookie::queue('email', $request->email, -360000);
            Cookie::queue('password', $request->password, -360000);
        }
        
        // return Cookie::get('user_email');
        if (isset(Auth::user()->_id)) {
            return redirect('/login');
        } else {
            $LoginUser = User::where('email', $request->email)->first();
//            print_r($LoginUser);exit;
            $cnt = count((array)$LoginUser);
            if ($cnt != 0) {
                $pass = $LoginUser->password;
                $role = $LoginUser->RoleID;
//              echo "avail";die;
                if ($role == '5bf3f8742b540c7f0c265a4e') {
                   if (Hash::check($request->password, $pass)) {
//                        echo "passmatch";die;
                        if ($LoginUser->IsActive == 'true') {
//                          print_r($LoginUser->_id);exit;
                            Auth::loginUsingId($LoginUser->_id);
                            return redirect('/');
                        } else {
                            if ($LoginUser->IsActive == 'false') {
                                Session::flash('message', 'Your account is inactive');
                                return redirect('/login');
                            }
                        }
                    } else {
                        Session::flash('message', 'The email/password combination is not valid. Please try again.');
                        return redirect('/login');
                    }
                } else {
                    Session::flash('message', 'Your Login is restricted. Login is only valid for super admin');
                    return redirect('/login');
                }
            } else {
                Session::flash('message', 'The email/password combination is not valid. Please try again.');
                return redirect('/login');
            }
        }
    }

}
