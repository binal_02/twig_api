<?php

namespace App\Http\Controllers\Twiggame;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\LoginRequest;
use Auth;
use Hash;
use App\User;
use Session;
use Cookie;
use App\TwigSession;
use App\UserSession;
use Illuminate\Http\Response;
use MongoDB\BSON\ObjectID;
use Carbon\Carbon;
Use Mail;

class ForgotPasswordController extends Controller
{

    public function forgotPassword(Request $request)
    {
        $success = $error = '';
            if($request->email == ''){
               $error = 'The email field is required';
               return response()->json(['errors' => $error], 400);
            }
        
            $sessionId = $request->sessionId;
            $sessionID = New ObjectID($sessionId);
            $session = TwigSession::where('_id', $sessionID)->where('IsDeleted','0')->first();
            $SessionStartTime = $session['StartTimeLong'];
            
            if(isset($session) && $session != ''){
                date_default_timezone_set("UTC");
                
                    $UserSession = UserSession::where('SessionID', $sessionID)->where('IsDeleted','0')->get();
                    $userInfo = User::where('email', $request->email)->first();
                    if(!empty($userInfo)){
                        $userId = $userInfo->_id;
                    }
                    if(!empty($UserSession)){
                        for($i=0; $i<count($UserSession); $i++){
                            $userIdOfSession = $UserSession[$i]->UserID;
                           
                            if($userIdOfSession == $userId){
                               //send mail
                                $this->email = $request->email;
                                $token = str_random(64);
                                $tokenarray[] = $token;
                                Mail::send('emails/resetPassword', ['userInfo' => $userInfo,'session' => $session,'tokenarray' => $tokenarray], function ($message) {
                                    $message->from('noreply@e.payt.com.au', 'TWIG GAME - Reset Password');
                                    $message->subject('Password Reset');
                                    $message->to($this->email);
                                });
                                DB::table('reset_password')->insert(
                                    ['email' => $request->email, 'token' => $token, 'isExpire' => false, 'created_at' => date('Y-m-d H:i:s')]
                                );
                                 $success = "A Password reset link has been sent on your email.";
                                 return response()->json(['success' => $success], 200 );
                            }else{
                                $error = "We can't find a user with that e-mail address.";
                                 return response()->json(['errors' => $error], 400 );
                            } 
                        }
                }else{
                    $error = 'SessionId is not valid,Please try again';
                    return response()->json(['errors' => $error], 400 );
                }
            }else{
                $error = 'SessionId is not valid,Please try again';
                return response()->json(['errors' => $error], 400 );
            }

    }
}
