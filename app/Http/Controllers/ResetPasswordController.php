<?php

namespace App\Http\Controllers\Twiggame;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Password;
use DB;
use Carbon\Carbon;

class ResetPasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */


    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
//    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    public function messages() {
        $messages = [
//            'email.required' => 'Please enter email',
//            'email.email' => 'Please enter valid email',
//            'password.required' => 'Please enter password',
            'password.regex' => 'The password format is invalid. Password must include at least one upper case letter, one lower case letter, and one numeric digit.',
        ];

        return $messages;
    }

    protected function rules() {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6|max:18|regex:/(^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,18}$)/u',
        ];
    }

    public function showResetForm(Request $request, $token = null) {

        $expireToken = DB::table('password_resets')
                ->where('email', $request->email)
                ->where('created_at', '>', Carbon::now()->subMinute(60))
                ->get();

        $user = DB::table('reset_password')->where('token', $request->token)
                ->where('isExpire', true)
                ->first();
        if (count($expireToken) > 0) {
            if (empty($user)) {
                return view('auth.passwords.reset')->with(
                                ['token' => $token, 'email' => $request->email]
                );
            } else {
                return view('auth.passwords.reset');
            }
        } else {
            return view('auth.passwords.expire');
        }

    }

    public function reset(Request $request) {

        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
                $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        }
        );
//        exit;
        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        switch ($response) {
            case Password::PASSWORD_RESET:
                DB::table('reset_password')->insert(
                        ['email' => $request->email, 'token' => $request->token, 'isExpire' => true, 'created_at' => date('Y-m-d H:i:s')]
                );
                return redirect('/login')->with('status', 'Your password has been reset successfully.');
            default:
                return redirect()->back()
                                ->withInput($request->only('email'))
                                ->withErrors(['email' => trans($response)]);
        }
    }

    protected function resetPassword($user, $password) {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();
    }

    protected function sendResetResponse($response) {
        return redirect($this->redirectPath())
                        ->with('status', trans($response));
    }

}
