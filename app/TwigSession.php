<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TwigSession extends Eloquent
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $connection = 'mongodb';
    protected $collection = 'TwigSession';
    protected $fillable = [
        'Name','StartTime','EndTime','SessionType','IsActive','CreatedBy','CreatedDate','ModifiedBy','ModifiedDate','IsDeleted','Location','StartTimeLong','EndTimeLong',
    ];    
    
}
