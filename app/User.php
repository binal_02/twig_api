<?php

namespace App;

 use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Notifications\PasswordReset; 
use MongoDB\BSON\ObjectID;
//{

class User extends Eloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable; 

//    use Notifiable;
   protected $connection = 'mongodb';
   protected $collection = 'User';
   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   
   
   protected $fillable = [
       'email', 'password','FirstName','LastName','AvatarURL'
   ];
   
   
    public function isAdmin()
    {
        if ($this->RoleID=='5bf3f8742b540c7f0c265a4e') {
            return true;
        }else{
            return false;
        }
        //echo $this->role;die;
        //return $this->role; // this looks for an admin column in your users table
    }
    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token, $this->FirstName));
    }
    
    public static function getUsersFromGroupId($gid) {
        $users = User::where('GroupID', new ObjectID($gid))->get();
        return $users;
    }
    public static function getGroupFromUserId($uid) {
        $users = User::where('_id', new ObjectID($uid))->first();
        return $users;
    }

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
   protected $hidden = [
       'password', 'remember_token',
   ];
}