<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDB\BSON\ObjectID;

class Role extends Eloquent
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $connection = 'mongodb';
    protected $collection = 'Role';
    
    protected $fillable = [
       'Name','CreatedBy','ModifiedBy','IsDeleted','ModifiedDate'
    ];    
    
    public static function gerRoleName($roleId) {
        $name = Role::where('_id', new ObjectID($roleId))->first();
        $cnt = count((array)$name);
            if ($cnt != 0) {
                return $name = $name->Name;
            }
    }
}
