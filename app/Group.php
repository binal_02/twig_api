<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDB\BSON\ObjectID;

class Group extends Eloquent
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $connection = 'mongodb';
    protected $collection = 'Group';
//    protected $dates = ['created_at', 'updated_at', 'datetime'];
    protected $fillable = [
        'Name','CreatedBy','CreatedDate','ModifiedBy','ModifiedDate','IsDeleted'
    ];    
    public static function getGroupFromGroupId($gid) {
        $group = Group::where('_id', new ObjectID($gid))->first();
        return $group['Name'];
    }
}
