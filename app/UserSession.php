<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDB\BSON\ObjectID;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Mail;

class UserSession extends Eloquent
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    use SoftDeletes;
    
    protected $connection = 'mongodb';
    protected $collection = 'UserSession';
    protected $fillable = [
        'SessionID','GroupID','UserID','UserSessionID','IsInvited','ReInvited','CreatedBy','CreatedDate','ModifiedBy','ModifiedDate','IsDeleted','Password',
    ];    
    
    public static function getUserSessionFromSessionId($sid) {
        $Sessions = UserSession::where('SessionID', new ObjectID($sid))->where('IsDeleted','0')->get();
        return $Sessions;
    }
    
    public static function sentUserInvitationMail($UserId,$GroupId){
        $userInfo = User::getGroupFromUserId($UserId);
        $UsersessionInfo = UserSession::where('UserID', new ObjectID($UserId))->where('GroupID', new ObjectID($GroupId))->where('IsDeleted','0')->get();
        
        if(!empty($UsersessionInfo)){
            for($i=0;$i<count($UsersessionInfo);$i++){
                $session = TwigSession::where('_id', new ObjectID($UsersessionInfo[$i]['SessionID']))->where('IsDeleted','0')->first();
                $UserSessionIDarray[] = $UsersessionInfo['0']->UserSessionID;
                //Send User Invitation mail
                Mail::send('emails/userInvite', ['userInfo' => $userInfo,'session' => $session,'UserSessionIDarray' => $UserSessionIDarray], function ($message) use ($session,$userInfo){
                    $message->from('noreply@e.payt.com.au', 'User Invitation Mail');
                    $message->subject('Invitation TWiG : '.$session->Name.' - '.$session->StartTime);
                    $message->to($userInfo['email']);
                });
            }
        }
    }
    
}
