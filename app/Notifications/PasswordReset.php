<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordReset extends Notification
{
    use Queueable;
    public $token;
    public $FirstName;
   
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token,$FirstName)
    {
        $this->token = $token;$this->FirstName = $FirstName;
    }   

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        return (new MailMessage)
        ->greeting('Hello '.$this->FirstName)
        ->line('You are receiving this email because we received a password reset request for your account.') // Here are the lines you can safely override
        ->action('Reset Password', url('password/reset', $this->token).'?email='.urlencode($notifiable->email))
        ->line('If this was a mistake, just ignore this mail and nothing will happen.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
